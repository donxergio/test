package education.kernet.konectia.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import education.kernet.konectia.KonectiaApp;
import education.kernet.konectia.R;
import education.kernet.konectia.model.User;
import education.kernet.konectia.networking.HTTPClient;
import education.kernet.konectia.networking.WSEndpoints;
import education.kernet.konectia.utils.ApplicationHolder;

public class LoginActivity extends AppCompatActivity {

    private KonectiaApp app;

    private EditText mUserView;
    private EditText mPasswordView;
    private TextView mVersionCodeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUserView = (EditText) findViewById(R.id.user);
        mPasswordView = (EditText) findViewById(R.id.password);

        mUserView.setText("Android");
        mPasswordView.setText("12345");


        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        mVersionCodeTextView = (TextView) findViewById(R.id.versionCode);
        String versionNumber;
        try {
            versionNumber = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
            mVersionCodeTextView.setText(getString(R.string.version_tag) + " " + versionNumber);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void attemptLogin() {
        String user = mUserView.getText().toString();
        String password = mPasswordView.getText().toString();

        User userToLogin = new User(user, password);
        UserLoginTask mTask = new UserLoginTask(this, userToLogin);
        mTask.execute((Void) null);
    }

    public class UserLoginTask extends AsyncTask<Void, Void, User> {

        private final User mUserToLogin;
        private final Context mContext;

        UserLoginTask(Context context, User user) {
            mContext = context.getApplicationContext();
            mUserToLogin = user;
        }

        @Override
        protected User doInBackground(Void... params) {
            String loginJson = createJSON(mUserToLogin);
            try{
                HTTPClient client = new HTTPClient();
                User returnedLoginUser = null;
                returnedLoginUser = client.loginPost(WSEndpoints.LOGIN, loginJson);

                if (returnedLoginUser != null) {
                    return returnedLoginUser;
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(final User loginUser) {

            if (loginUser != null && checkPassword(loginUser)) {
                /*
                KonectiaApp app = ApplicationHolder.getApplication();
                app.setWorkingUser(loginUser);
                */

                Intent myIntent = new Intent(mContext, TabsActivity.class);
                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myIntent);
                finish();
            } else {
                mPasswordView.setError("Password incorrecto");
                mPasswordView.requestFocus();
            }
        }

        private boolean checkPassword(User loginUser) {
            if (mUserToLogin.getName().equalsIgnoreCase(loginUser.getName())
                && mUserToLogin.getPassword().equalsIgnoreCase(loginUser.getPassword())) {
                return true;
            } else {
                return false;
            }
        }

        private String createJSON(User user){
            Gson gson = new GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation()
                    .create();
            return gson.toJson(user);
        }
    }
}
