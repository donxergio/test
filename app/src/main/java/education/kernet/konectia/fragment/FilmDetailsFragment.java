package education.kernet.konectia.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import education.kernet.konectia.KonectiaApp;
import education.kernet.konectia.R;
import education.kernet.konectia.caller.IOnFilmToFavorites;
import education.kernet.konectia.model.Film;
import education.kernet.konectia.utils.ApplicationHolder;


public class FilmDetailsFragment extends Fragment {

    public static final String FILM_TYPE_REQUEST = "FILM_REQUEST";

    private IOnFilmToFavorites caller;
    private KonectiaApp app;
    private Film film;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IOnFilmToFavorites) {
            caller = (IOnFilmToFavorites) context;
        } else {
            throw new ClassCastException (context.toString()
            + " must implement IOnFilmToFavorites");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = ApplicationHolder.getApplication();
        film = app.getWorkingFilm();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_film, container, false);

        setUpViews(view);
        return view;
    }

    private void setUpViews(View view) {
        TextView titleTextView = (TextView) view.findViewById(R.id.film_title);
        titleTextView.setText(film.getTitle());

        TextView detailsTextView = ((TextView) view.findViewById(R.id.film_details));
        detailsTextView.setText(film.getRated() + " | " + film.getRuntime() + " | " +
                film.getGenre() + " | " + film.getReleased());

        ((TextView) view.findViewById(R.id.film_director)).setText(film.getDirector());
        ((TextView) view.findViewById(R.id.film_actors)).setText(film.getActors());
        ((TextView) view.findViewById(R.id.film_writers)).setText(film.getWriter());
        ((TextView) view.findViewById(R.id.film_plot)).setText(film.getPlot());
        ((TextView) view.findViewById(R.id.film_awards)).setText(film.getAwards());

        ImageView poster = (ImageView) view.findViewById(R.id.film_poster);
        Glide.with(getContext()).load(film.getPoster()).into(poster);

    }
}
