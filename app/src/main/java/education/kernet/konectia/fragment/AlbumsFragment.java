package education.kernet.konectia.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import education.kernet.konectia.KonectiaApp;
import education.kernet.konectia.R;
import education.kernet.konectia.activity.DetailsActivity;
import education.kernet.konectia.activity.TabsActivity;
import education.kernet.konectia.adapter.AlbumListAdapter;
import education.kernet.konectia.adapter.FilmListAdapter;
import education.kernet.konectia.caller.IAlbumsDownloadDone;
import education.kernet.konectia.caller.IFilmsDownloadDone;
import education.kernet.konectia.caller.IOnAlbumClickListener;
import education.kernet.konectia.caller.IOnFilmClickListener;
import education.kernet.konectia.model.Album;
import education.kernet.konectia.model.Film;
import education.kernet.konectia.task.AlbumsDownloadTask;
import education.kernet.konectia.task.FilmsDownloadTask;
import education.kernet.konectia.utils.ApplicationHolder;

import static education.kernet.konectia.fragment.AlbumDetailsFragment.ALBUM_TYPE_REQUEST;


public class AlbumsFragment extends Fragment implements IAlbumsDownloadDone {

    private RecyclerView recyclerView;
    private AlbumListAdapter albumListAdapter;
    private TextView emptyTextView;
    private KonectiaApp app;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = ApplicationHolder.getApplication();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_albums, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.album_list);
        emptyTextView = (TextView) view.findViewById(R.id.empty_view);

        loadData();

        return view;
    }

    private void loadData() {
        AlbumsDownloadTask albumsDownloadTask = new AlbumsDownloadTask((TabsActivity) getActivity(), AlbumsFragment.this);
        albumsDownloadTask.execute();
    }

    @Override
    public void processResult(ArrayList<Album> albumArrayList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        albumListAdapter = new AlbumListAdapter(albumArrayList, getActivity(), new IOnAlbumClickListener() {
            @Override
            public void onItemClick(Album item) {
                app.setWorkingAlbum(item);
                callDetailsActivity();
            }
        });

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(albumListAdapter);

        if (albumArrayList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyTextView.setText(getString(R.string.no_data_available));
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.GONE);
        }
    }

    private void callDetailsActivity() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(DetailsActivity.EXTRA_REQUEST_TYPE, ALBUM_TYPE_REQUEST);
        startActivity(intent);
    }
}
