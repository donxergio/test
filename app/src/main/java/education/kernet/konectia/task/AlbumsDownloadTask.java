package education.kernet.konectia.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import education.kernet.konectia.R;
import education.kernet.konectia.activity.TabsActivity;
import education.kernet.konectia.caller.IAlbumsDownloadDone;
import education.kernet.konectia.fragment.AlbumsFragment;
import education.kernet.konectia.json.JSONDeserializer;
import education.kernet.konectia.model.Album;
import education.kernet.konectia.networking.HTTPClient;
import education.kernet.konectia.networking.Keys;
import education.kernet.konectia.networking.WSEndpoints;
import okhttp3.Response;


public class AlbumsDownloadTask extends AsyncTask<Void, String, String> {
    private Activity activity;
    private IAlbumsDownloadDone caller;
    private ProgressDialog dialog;
    private boolean forceCancel;
    private ArrayList<Album> albumsResult;
    private WeakReference<TabsActivity> context;

    private boolean failure;

    public AlbumsDownloadTask(TabsActivity activity, AlbumsFragment caller) {
        this.activity = activity;
        this.caller = caller;
        this.context = new WeakReference<>(activity);

        this.albumsResult = new ArrayList<>();
        this.failure = false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        this.dialog = ProgressDialog.show(activity
                , activity.getString(R.string.film_sync)
                , activity.getString(R.string.sync_msg)
                , true
                , true
                , new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if (AlbumsDownloadTask.this.isCancelled()) {
                            AlbumsDownloadTask.this.forceCancel = true;
                        } else {
                            AlbumsDownloadTask.this.cancel(true);
                        }
                    }
                });
        this.dialog.setCanceledOnTouchOutside(false);
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            fetchData();
        } catch (Exception e) {
            e.printStackTrace();
            failure = true;
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        if (activity != null && !activity.isFinishing()) {
            this.dialog.cancel();

            if (!failure) {
                caller.processResult(albumsResult);
            } else {
                warningDialog();
            }
        }
    }

    private void fetchData() {
        try {
            Response response;
            HTTPClient client = new HTTPClient();
            response = client.get(WSEndpoints.ALBUMS);

            if (response.isSuccessful() && !response.toString().isEmpty()) {
                parseResponse(response, Keys.ALBUMS);
            }
        } catch (Exception e) {
            e.printStackTrace();
            failure = true;
        }
    }

    private void parseResponse(Response response, String key) {
        try {
            String responseString;
            responseString = response.body().string();
            response.close();

            albumsResult = JSONDeserializer.deserializeList(responseString, Album.class, key);

        } catch (Exception e) {
            e.printStackTrace();
            failure = true;
        }
    }

    private void warningDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.context.get());

        builder.setTitle(activity.getString(R.string.download_not_complete));
        builder.setMessage(activity.getString(R.string.sync_failure));

        builder.setPositiveButton(
                activity.getString(R.string.accept),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
