package education.kernet.konectia.networking;

public class WSEndpoints {

    public static final String LOGIN = "login.json";
    public static final String FILMS = "films.json";
    public static final String ALBUMS = "albums.json";
}
