package education.kernet.konectia.utils;


import education.kernet.konectia.KonectiaApp;

public class ApplicationHolder {

    private static KonectiaApp application;

    public static KonectiaApp getApplication() {
        return application;
    }

    public static void setApplication(KonectiaApp application) {
        ApplicationHolder.application = application;
    }

}
