package education.kernet.konectia.model;


import com.google.gson.annotations.SerializedName;

public class Album {

    @SerializedName("Title")
    private String title;

    @SerializedName("Year")
    private String year;

    @SerializedName("USChart")
    private String usChart;

    @SerializedName("Released")
    private String released;

    @SerializedName("Label")
    private String label;

    @SerializedName("CoverURL")
    private String coverURL;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getUsChart() {
        return usChart;
    }

    public void setUsChart(String usChart) {
        this.usChart = usChart;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCoverURL() {
        return coverURL;
    }

    public void setCoverURL(String coverURL) {
        this.coverURL = coverURL;
    }
}
