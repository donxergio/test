package education.kernet.konectia.json;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class JSONDeserializer {

    private static final String IDS = "ids";

    private JSONDeserializer() {
    }

    public static <T> T deserialize(String responseString, Class<T> type) {

        JsonElement jelement = new JsonParser().parse(responseString);
        JsonObject jobject = jelement.getAsJsonObject();

        Gson gson = new Gson();
        T object = gson.fromJson(jobject, type);
        return object;
    }

    public static <T> ArrayList<T> deserializeList(String responseString, Class<T> type, String key) {

        ArrayList<T> items = new ArrayList<T>();
        try {
            JsonElement jelement = new JsonParser().parse(responseString);
            JsonObject itemObject = jelement.getAsJsonObject();

            JsonArray itemArray = itemObject.getAsJsonArray(key);

            if (itemArray != null) {
                for (JsonElement listItemNode : itemArray) {
                    T item = deserialize(listItemNode.toString(), type);
                    items.add(item);
                }
            }
            return items;
        } catch (Exception e) {
            e.printStackTrace();
            return items;
        }
    }

    public static List<String> deserializeIds(String responseString) {

        List<String> items = new ArrayList<String>();
        try {
            JsonElement jelement = new JsonParser().parse(responseString);
            JsonObject jObj = jelement.getAsJsonObject();
            JsonArray itemArray = jObj.getAsJsonArray(IDS);

            if (itemArray != null) {
                for (JsonElement listItemNode : itemArray) {
                    items.add(listItemNode.getAsString());
                }
            }
            return items;
        } catch (Exception e) {
            e.printStackTrace();
            return items;
        }
    }

    public static Map<String, Object> deserializeRaw(String responseString) {
        Gson gson = new Gson();
        Map<String, Object> decodedRaw = gson.fromJson(responseString, new TypeToken<Map<String, Object>>() {
        }.getType());
        return decodedRaw;
    }
}
