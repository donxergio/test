package education.kernet.konectia.caller;


import java.util.ArrayList;

import education.kernet.konectia.model.Film;

public interface IFilmsDownloadDone {
    void processResult(ArrayList<Film> movieArrayList);
}
