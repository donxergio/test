package education.kernet.konectia.caller;


import education.kernet.konectia.model.Film;

public interface IOnFilmClickListener {
    void onItemClick(Film item);
}
