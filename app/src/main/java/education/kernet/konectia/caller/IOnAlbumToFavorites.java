package education.kernet.konectia.caller;


import education.kernet.konectia.model.Album;

public interface IOnAlbumToFavorites {
    void onAlbumToFavorites(Album album);
}
