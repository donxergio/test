package education.kernet.konectia.caller;


import education.kernet.konectia.model.Film;

public interface IOnFilmToFavorites {
    void onFilmToFavorites(Film film);
}
